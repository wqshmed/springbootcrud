package com.waqasahmed.edu.controller.model.dao;

import com.waqasahmed.edu.controller.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentRepository extends CrudRepository <Student, Long> {

}
