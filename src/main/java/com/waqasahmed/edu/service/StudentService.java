package com.waqasahmed.edu.service;

import com.waqasahmed.edu.controller.model.Student;

import java.util.List;

public interface StudentService {
    /**
     * Get all students
     * @return
     */
    public List<Student> getAllStudents();

    /**
     * save student
     * @param student
     * @return
     */
    public Student addStudent(Student student);
}
