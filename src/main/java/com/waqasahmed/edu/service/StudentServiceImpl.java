package com.waqasahmed.edu.service;

import com.waqasahmed.edu.controller.model.Student;
import com.waqasahmed.edu.controller.model.dao.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class StudentServiceImpl implements StudentService{
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        List<Student> list = new ArrayList<>();
        studentRepository.findAll().forEach(i -> list.add(i));
        return list;
    }

    @Override
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }
}
