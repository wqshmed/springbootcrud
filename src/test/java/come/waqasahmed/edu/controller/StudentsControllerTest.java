package come.waqasahmed.edu.controller;

import com.waqasahmed.edu.controller.StudentController;
import com.waqasahmed.edu.controller.model.Student;
import com.waqasahmed.edu.service.StudentService;
import com.waqasahmed.edu.service.StudentServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StudentsControllerTest {

    @InjectMocks
    private StudentController studentController;

    @Mock
    StudentService studentService;

    @Test
    public void testGetStudents()
    {
        Student student1 = new Student(1, "waqas", "ahmed", "11111111", "waqas.xhmed@gmail.com");
        Student student2 = new Student(2, "waqas", "ahmed", "22222222", "waqas.xhmed@gmail.com");
        List<Student> list = new ArrayList<Student>();
        list.addAll(Arrays.asList(student1, student2));

        when(studentService.getAllStudents()).thenReturn(list);

        List<Student> result = studentController.getStudents();

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).getFirstName()).isEqualTo(student1.getFirstName());
        assertThat(result.get(1).getFirstName()).isEqualTo(student2.getFirstName());
    }

    @Test
    public void testAddStudent()
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Student student = new Student();
        student.setId(1);
        when(studentService.addStudent(Mockito.any(Student.class))).thenReturn(student);

        Student studentToAdd = new Student(1, "Waqas", "Ahmed", "1212121212", "waqas.xhmed@gmail.com");
        ResponseEntity<Object> responseEntity = studentController.addStudent(studentToAdd);

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
        assertThat(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
    }
}
