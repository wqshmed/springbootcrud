package come.waqasahmed.edu.controller;

import com.waqasahmed.edu.SpringBootCRUDApplication;
import com.waqasahmed.edu.controller.model.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootCRUDApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentsControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Sql({"classpath:schema.sql", "classpath:data.sql" })
    @Test
    public void testAllStudents()
    {
        assertTrue(
                this.restTemplate
                        .getForObject("http://localhost:" + port + "/students", ArrayList.class)
                        .size() == 5);
    }

    @Test
    public void testAddStudent() {
        Student student = new Student();
        student.setFirstName("Waqas");
        student.setLastName("Ahmed");
        student.setPhone("1212121");
        student.setEmail("waqas.xhmed@gmail.com");
        ResponseEntity<String> responseEntity = this.restTemplate .postForEntity("http://localhost:" + port + "/students", student, String.class);
        assertThat(201).isEqualTo(responseEntity.getStatusCodeValue());
    }
}
